Builds a docker container with the C++, Android, and Android Native development environments.
Android is located at /opt/adk.

Modify build.sh to push the container up under your own username.
